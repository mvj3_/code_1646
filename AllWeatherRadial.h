//
//  AllWeatherRadial.h
//  Learning0508
//
//  Created by  apple on 13-5-8.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Tire.h"

@interface AllWeatherRadial : Tire
{
    float rainHandling;
    float snowHandling;
    NSString * name;
    NSString * type;
    float changeName;
}
-(id)initWithRH:(float)rainValue sH:(float)snowValue
       pressure:(float) pressureValue treadDepth:(float) treadDepthValue;

//使用特性， @property，自动声明属性的setter和getter方法
@property float rainHandling;
@property float snowHandling;

//赋值
@property (assign) NSString * type;
//复制对象
@property (copy) NSString * name;
//保留对象
//@property (retain) Tire * tire;

//改变名称
@property float hello;

@end
